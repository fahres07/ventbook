package com.nulfa.ventbook

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nulfa.ventbook.databinding.ActivityListTicketBinding

class ListTicketActivity : AppCompatActivity() {

    private val mAdapter = TicketAdapter()
    private lateinit var binding : ActivityListTicketBinding
    private lateinit var dbRef : DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListTicketBinding.inflate(layoutInflater)
        setContentView(binding.root)

        try {
            dbRef = FirebaseDatabase.getInstance().reference
            val userid = FirebaseAuth.getInstance().currentUser?.uid.toString()
            dbRef.child("Ticket").child(userid).addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val listTicket = arrayListOf<Ticket>()

                        snapshot.children.forEach { tickets ->
                            tickets.children.forEach {
                                it.getValue(TicketResponse::class.java)?.let { response ->
                                    val transformedResponse = mapResponseToTicket(response)
                                    listTicket.add(transformedResponse)
                                }
                            }
                        }

                        mAdapter.setData(listTicket)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Toast.makeText(this@ListTicketActivity, error.message, Toast.LENGTH_SHORT).show()
                    }

                }
            )

        } catch (e:Exception){
            Log.d("debug", e.message.toString())
        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        with(binding.rvListTicket){
            layoutManager = LinearLayoutManager(this@ListTicketActivity)
            adapter=mAdapter
        }
    }

    private fun mapResponseToTicket(response: TicketResponse): Ticket =
        Ticket(
            kode_ticket = response.kode_ticket?:"",
            id_user = response.id_user?: "",
            txt_judul = response.txt_judul ?: "",
            txt_waktu = response.txt_waktu ?: "",
            txt_tempat = response.txt_tempat ?: "",
            txt_order = response.txt_order ?: "",
            status_bayar = response.status_bayar ?:"",
            methode_bayar = response.methode_bayar ?:"",
            txt_bayar = response.txt_bayar ?:""
        )

    companion object {
        const val TAG_TICKET = "TAG_TICKET"
    }
}
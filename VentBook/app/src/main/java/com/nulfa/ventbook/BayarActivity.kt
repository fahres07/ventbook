package com.nulfa.ventbook

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.nulfa.ventbook.DetailEventActivity.Companion.TAG_EVENT_DETAIL
import com.nulfa.ventbook.DetailEventActivity.Companion.TAG_TIKET
import com.nulfa.ventbook.databinding.ActivityBayarBinding

class BayarActivity : AppCompatActivity() {
    lateinit var database : DatabaseReference
    private lateinit var binding : ActivityBayarBinding
    private var methodeBayar  : String = ""
    private var txt_waktu  : String = ""
    private var txt_tempat  : String = ""
    private var txt_order  : String = ""
    private var txt_judul  : String = ""
    private var status_bayar  : String = "On Proccess"
    private var kode_ticket  : String = ""
    private var txt_bayar  : String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityBayarBinding.inflate(layoutInflater)
        setContentView(binding.root)
        intent.getStringExtra(TAG_TIKET)?.let {
            binding.edtJmlhTiket.setText(it)
        }
        intent.getParcelableExtra<Event>(TAG_EVENT_DETAIL)?.let {

            txt_judul = it.txt_judul
            txt_waktu = it.txt_waktu
            txt_tempat = it.txt_tempat
            txt_order = binding.edtJmlhTiket.text.toString()
            binding.txtJudulBayar.text = txt_judul
            binding.txtTotalHargaTicket.text = "Rp."+(binding.edtJmlhTiket.text.toString().toInt()*it.txt_harga.toInt()).toString()
            txt_bayar = (binding.edtJmlhTiket.text.toString().toInt()*it.txt_harga.toInt()).toString()
        }

        binding.btnBuyNow.setOnClickListener {
            database = FirebaseDatabase.getInstance().getReference("Ticket")

            FirebaseAuth.getInstance().currentUser?.uid?.let { userid ->
                kode_ticket = database.child(userid).child(status_bayar).push().key ?: ""
                val ticket = Ticket(kode_ticket,userid, txt_judul, txt_waktu, txt_tempat, txt_order, status_bayar, methodeBayar,  txt_bayar)
                database.child(userid).child(status_bayar).child(kode_ticket).setValue(ticket)
                    .addOnSuccessListener {
                        val intent = Intent(this, DetailBayarActivity::class.java).putExtra(
                            TAG_BAYAR_TICKET, ticket)
                        startActivity(intent)
                    }.addOnFailureListener {
                        Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                    }
            }
        }
    }

    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            val checked = view.isChecked


            when (view.getId()) {
                R.id.radio_1 ->
                    if (checked) {
                        methodeBayar = "DANA"
                    }
                R.id.radio_2 ->
                    if (checked) {
                        methodeBayar = "GoPay"
                    }
                R.id.radio_3 ->
                    if (checked) {
                        methodeBayar = "ShopeePay"
                    }
                R.id.radio_4 ->
                    if (checked) {
                        methodeBayar = "OVO"
                    }
            }
        }
    }

    companion object {
        const val TAG_BAYAR_TICKET="TAG_BAYAR_TICKET"
    }
}
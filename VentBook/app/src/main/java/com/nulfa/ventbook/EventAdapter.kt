package com.nulfa.ventbook

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nulfa.ventbook.MainActivity.Companion.TAG_EVENT
import com.nulfa.ventbook.databinding.RowRvOthersBinding

class EventAdapter : RecyclerView.Adapter<EventAdapter.EventViewHolder>() {
    private val data = arrayListOf<Event>()

    fun setData(newData: List<Event>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    inner class EventViewHolder(private val view: RowRvOthersBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(data: Event) {
            Glide.with(itemView).load(data.gambar).into(view.ivOther)
            view.tvJudulEvent.text = data.txt_judul
            view.tvWaktuEvent.text = data.txt_waktu
            view.tvTempatEvent.text = data.txt_tempat
            view.tvTanggalEvent.text = data.txt_tanggal
            view.root.setOnClickListener {
                itemView.context.startActivity(
                    Intent(itemView.context, DetailEventActivity::class.java).putExtra(
                        TAG_EVENT,
                        data
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder =
        EventViewHolder(
            RowRvOthersBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
}
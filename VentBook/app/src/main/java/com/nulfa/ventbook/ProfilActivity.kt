package com.nulfa.ventbook

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.nulfa.ventbook.databinding.ActivityProfilBinding

class ProfilActivity : AppCompatActivity() {

    lateinit var bottom_navigation_view : BottomNavigationView
    private lateinit var binding: ActivityProfilBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfilBinding.inflate(layoutInflater)
        setContentView(binding.root)

        FirebaseAuth.getInstance().currentUser?.displayName?.let { username ->
            binding.txtUserName.text = username
        }

        FirebaseAuth.getInstance().currentUser?.email?.let { email ->
            binding.txtEmail.text = email
        }

        FirebaseAuth.getInstance().currentUser?.photoUrl?.let { photo ->
            Glide.with(this).load(photo).into(binding.imageView)
        }

        binding.btnLogoutUser.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build()
            var googleSignInClient = GoogleSignIn.getClient(this, gso)
            googleSignInClient.signOut()
            val intent = Intent(this , LoginActivity::class.java).also { intent ->
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            startActivity(intent)
        }


        bottom_navigation_view = findViewById(R.id.bottom_navigation_view_new)
        bottom_navigation_view.getMenu().findItem(R.id.itemProfil).setChecked(true);
        bottom_navigation_view.setOnNavigationItemSelectedListener(menuItemSelected)

    }

    fun ticketOnclick(v: View){
        val intent = Intent(this, ListTicketActivity::class.java)
        startActivity(intent)
    }

    private val menuItemSelected = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.itemHome ->{
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0,0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.itemTicket ->{
                val intent = Intent(this, EventActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0,0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.itemProfil ->{
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


}
package com.nulfa.ventbook

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nulfa.ventbook.databinding.ActivityEventBinding

class EventActivity : AppCompatActivity() {
    private val mAdapter = EventAdapter()
    lateinit var bottom_navigation_view : BottomNavigationView
    private lateinit var binding : ActivityEventBinding
    private lateinit var dbRef : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityEventBinding.inflate(layoutInflater)
        setContentView(binding.root)

        try {
                dbRef = FirebaseDatabase.getInstance().reference
                dbRef.child("Event").addValueEventListener(
                    object : ValueEventListener {
                        override fun onDataChange(snapshot: DataSnapshot) {
                            val listEvent = arrayListOf<Event>()

                            snapshot.children.forEach {
                                it.getValue(EventResponse::class.java)?.let { response ->
                                    val transformedResponse = mapResponseToLink(response)
                                    listEvent.add(transformedResponse)
                                }
                            }

                            mAdapter.setData(listEvent)
                        }

                        override fun onCancelled(error: DatabaseError) {
                            Toast.makeText(this@EventActivity, error.message, Toast.LENGTH_SHORT).show()
                        }

                    }
                )

        } catch (e:Exception){
            Log.d("debug", e.message.toString())
        }
        setupRecyclerView()

        bottom_navigation_view = findViewById(R.id.bottom_navigation_view_new)
        bottom_navigation_view.getMenu().findItem(R.id.itemTicket).setChecked(true);
        bottom_navigation_view.setOnNavigationItemSelectedListener(menuItemSelected)
    }

    private fun setupRecyclerView() {
        with(binding.rvItemEvent){
            layoutManager = LinearLayoutManager(this@EventActivity)
            adapter=mAdapter
        }
    }

    private fun mapResponseToLink(response: EventResponse): Event =
        Event(
            gambar = response.gambar ?: "",
            txt_judul = response.txt_judul ?: "",
            txt_waktu = response.txt_waktu ?: "",
            txt_tempat = response.txt_tempat ?: "",
            txt_harga = response.txt_harga ?: "",
            txt_tanggal = response.txt_tanggal ?: "",
            txt_desc =  response.txt_desc ?: ""
        )

    private val menuItemSelected = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.itemHome ->{
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0,0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.itemTicket ->{

                return@OnNavigationItemSelectedListener true
            }
            R.id.itemProfil ->{
                val intent = Intent(this, ProfilActivity::class.java)
                startActivity(intent)
                overridePendingTransition(0,0)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
}
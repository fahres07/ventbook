package com.nulfa.ventbook

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nulfa.ventbook.ListTicketActivity.Companion.TAG_TICKET
import com.nulfa.ventbook.databinding.RowRvTicketBinding

class TicketAdapter : RecyclerView.Adapter<TicketAdapter.TicketViewHolder>() {
    private val data = arrayListOf<Ticket>()

    fun setData(newData: List<Ticket>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    inner class TicketViewHolder(private val view: RowRvTicketBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(data: Ticket) {
            view.txtJudulBayar.text = data.txt_judul
            view.root.setOnClickListener {
                itemView.context.startActivity(
                    Intent(itemView.context, DetailTicketActivity::class.java).putExtra(TAG_TICKET,data)
                    )
            }
        }
    }

    override fun onBindViewHolder(holder: TicketViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketViewHolder =
        TicketViewHolder(
            RowRvTicketBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
}
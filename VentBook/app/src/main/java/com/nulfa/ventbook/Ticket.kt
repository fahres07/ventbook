package com.nulfa.ventbook

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Ticket(
    var kode_ticket : String,
    var id_user : String,
    var txt_judul: String,
    var txt_waktu: String,
    var txt_tempat: String,
    var txt_order : String,
    var status_bayar  :String,
    var methode_bayar  :String,
    var txt_bayar :String
): Parcelable

package com.nulfa.ventbook

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nulfa.ventbook.BayarActivity.Companion.TAG_BAYAR_TICKET
import com.nulfa.ventbook.databinding.ActivityDetailBayarBinding

class DetailBayarActivity : AppCompatActivity() {
    private lateinit var binding:ActivityDetailBayarBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBayarBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getParcelableExtra<Ticket>(TAG_BAYAR_TICKET)?.let {

            updateUI(it)
        }


        binding.btnOk.setOnClickListener {

            val intent = Intent(this, ProfilActivity::class.java)
            startActivity(intent)
        }
    }

    private fun updateUI (ticket: Ticket) {


        if (ticket.methode_bayar=="DANA") {
            binding.txtBayarMethod.text = ticket.methode_bayar
            binding.txtBayarMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_dana, 0, 0, 0)
            binding.txtNoMethod.text = "No ${ticket.methode_bayar}"
        } else if (ticket.methode_bayar=="GoPay") {
            binding.txtBayarMethod.text = ticket.methode_bayar
            binding.txtBayarMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_gopay, 0, 0, 0)
            binding.txtNoMethod.text = "No ${ticket.methode_bayar}"
        } else if (ticket.methode_bayar=="ShopeePay") {
            binding.txtBayarMethod.text = ticket.methode_bayar
            binding.txtBayarMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_shopeepay, 0, 0, 0)
            binding.txtNoMethod.text = "No ${ticket.methode_bayar}"
        } else if (ticket.methode_bayar=="OVO") {
            binding.txtBayarMethod.text = ticket.methode_bayar
            binding.txtBayarMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_ovo, 0, 0, 0)
            binding.txtNoMethod.text = "No ${ticket.methode_bayar}"
        }

        binding.jumlahTicket.text = "${ticket.txt_order} Tiket"
        binding.totalBayar.text = "Rp.${ticket.txt_bayar}"
    }
}
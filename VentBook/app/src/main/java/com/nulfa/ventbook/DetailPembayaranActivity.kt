package com.nulfa.ventbook

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.nulfa.ventbook.ListTicketActivity.Companion.TAG_TICKET
import com.nulfa.ventbook.databinding.ActivityDetailPembayaranBinding

class DetailPembayaranActivity : AppCompatActivity() {
    lateinit var binding : ActivityDetailPembayaranBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailPembayaranBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getParcelableExtra<Ticket>(TAG_TICKET)?.let {
            updateUI(it.methode_bayar)
            binding.jumlahTicket.text = it.txt_order
            binding.totalBayar.text = "Rp."+it.txt_bayar
        }

        binding.btnOk.setOnClickListener {
            val intent = Intent(this, ProfilActivity::class.java)
            startActivity(intent)
        }
    }

    private fun updateUI (paymentMethod : String) {


        if (paymentMethod=="DANA") {
            binding.txtBayarMethod.text = paymentMethod
            binding.txtBayarMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_dana, 0, 0, 0)
            binding.txtNoMethod.text = "No $paymentMethod"
        } else if (paymentMethod=="GoPay") {
            binding.txtBayarMethod.text = paymentMethod
            binding.txtBayarMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_gopay, 0, 0, 0)
            binding.txtNoMethod.text = "No $paymentMethod"
        } else if (paymentMethod=="ShopeePay") {
            binding.txtBayarMethod.text = paymentMethod
            binding.txtBayarMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_shopeepay, 0, 0, 0)
            binding.txtNoMethod.text = "No $paymentMethod"
        } else if (paymentMethod=="OVO") {
            binding.txtBayarMethod.text = paymentMethod
            binding.txtBayarMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_ovo, 0, 0, 0)
            binding.txtNoMethod.text = "No $paymentMethod"
        }
    }
}
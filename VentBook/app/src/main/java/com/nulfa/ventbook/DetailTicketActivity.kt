package com.nulfa.ventbook

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.nulfa.ventbook.ListTicketActivity.Companion.TAG_TICKET
import com.nulfa.ventbook.databinding.ActivityDetailTicketBinding

class DetailTicketActivity : AppCompatActivity() {
    lateinit var binding : ActivityDetailTicketBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailTicketBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getParcelableExtra<Ticket>(TAG_TICKET)?. let { updateUI(it) }
    }

    private fun updateUI(data : Ticket){
        binding.ticketJudul.text = data.txt_judul
        binding.waktuTicket.text = data.txt_waktu
        binding.tempatTicket.text = data.txt_tempat
        binding.jumlahTicket.text = data.txt_order
        binding.statusTicket.text = data.status_bayar

        binding.btnTransaksi.setOnClickListener {
            val intent = Intent(this, DetailPembayaranActivity::class.java).putExtra(TAG_TICKET,data)
            startActivity(intent)
        }
    }
}
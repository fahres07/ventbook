package com.nulfa.ventbook

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.nulfa.ventbook.MainActivity.Companion.TAG_EVENT
import com.nulfa.ventbook.databinding.ActivityDetailEventBinding

class DetailEventActivity : AppCompatActivity() {
    private lateinit var binding : ActivityDetailEventBinding
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding= ActivityDetailEventBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getParcelableExtra<Event>(TAG_EVENT)?.let { updateUI(it) }

    }

    private fun updateUI(data : Event){
        binding.txtJudulEvent.text = data.txt_judul
        binding.txtHargaEvent.text = "Rp."+data.txt_harga
        binding.txtTempatEvent.text = data.txt_tempat
        binding.txtWaktuEvent.text = data.txt_tanggal
        binding.txtWaktuEvent2.text = data.txt_waktu
        binding.detailEvent.text= data.txt_desc
        Glide.with(this).load(data.gambar).into(binding.gambar)
        binding.jmlhTiket.text.toString()

        binding.btnBuy.setOnClickListener {
            val intent = Intent(this, BayarActivity::class.java).putExtra(
                TAG_TIKET, binding.jmlhTiket.text.toString()).putExtra(
                TAG_EVENT_DETAIL, data)
            startActivity(intent)
        }
    }

    companion object {
        const val TAG_EVENT_DETAIL = "TAG_EVENT_DETAIL"
        const val TAG_TIKET = "TAG_TIKET"
    }

}
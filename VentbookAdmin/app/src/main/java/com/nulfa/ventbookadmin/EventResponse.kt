package com.nulfa.ventbookadmin

import com.google.gson.annotations.SerializedName

data class EventResponse(
    @field:SerializedName("gambar")
    val gambar: String? = null,
    @field:SerializedName("txt_judul")
    val txt_judul: String? = null,
    @field:SerializedName("txt_waktu")
    val txt_waktu: String? = null,
    @field:SerializedName("txt_tempat")
    val txt_tempat: String? = null,
    @field:SerializedName("txt_harga")
    val txt_harga: String? = null,
    @field:SerializedName("txt_tanggal")
    val txt_tanggal: String? = null,
    @field:SerializedName("txt_desc")
    val txt_desc: String? = null
)

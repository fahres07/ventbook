package com.nulfa.ventbookadmin

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.nulfa.ventbookadmin.EventActivity.Companion.TAG_EVENT
import com.nulfa.ventbookadmin.databinding.RowRvEventBinding

class EventAdapter : RecyclerView.Adapter<EventAdapter.EventViewHolder>() {
    private val data = arrayListOf<Event>()
    private lateinit var dbRef : DatabaseReference
    fun setData(newData: List<Event>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    inner class EventViewHolder(private val view: RowRvEventBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(data: Event) {
            Glide.with(itemView).load(data.gambar).into(view.imgPoster)
            view.txtTitle.text = data.txt_judul
            view.root.setOnClickListener {
                itemView.context.startActivity(
                    Intent(itemView.context, DetailEventActivity::class.java).putExtra(
                        TAG_EVENT,
                        data
                    )
                )
            }
            view.btnEdit.setOnClickListener{
                itemView.context.startActivity(
                    Intent(itemView.context, EditEventActivity::class.java).putExtra(
                        TAG_EVENT,
                        data
                    )
                )
            }
            view.btnDelete.setOnClickListener {
                dbRef = FirebaseDatabase.getInstance().getReference("Event")
                dbRef.child(data.txt_judul).removeValue().addOnSuccessListener {

                }.addOnFailureListener {

                }
            }
        }
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder =
        EventViewHolder(
            RowRvEventBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
}
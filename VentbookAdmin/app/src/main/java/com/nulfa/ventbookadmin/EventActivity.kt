package com.nulfa.ventbookadmin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import com.nulfa.ventbookadmin.databinding.ActivityEventBinding

class EventActivity : AppCompatActivity() {
    private val mAdapter = EventAdapter()
    private lateinit var dbRef : DatabaseReference
    private lateinit var binding: ActivityEventBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEventBinding.inflate(layoutInflater)
        setContentView(binding.root)

        try {
            dbRef = FirebaseDatabase.getInstance().reference
            dbRef.child("Event").addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val listEvent = arrayListOf<Event>()

                        snapshot.children.forEach {
                            it.getValue(EventResponse::class.java)?.let { response ->
                                val transformedResponse = mapResponseToLink(response)
                                listEvent.add(transformedResponse)
                            }
                        }
                        mAdapter.setData(listEvent)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Toast.makeText(this@EventActivity, error.message, Toast.LENGTH_SHORT).show()
                    }

                }
            )

        } catch (e:Exception){
            Log.d("debug", e.message.toString())
        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        with(binding.rvEvent){
            layoutManager = LinearLayoutManager(this@EventActivity)
            adapter=mAdapter
        }
    }

    private fun mapResponseToLink(response: EventResponse): Event =
        Event(
            gambar = response.gambar ?: "",
            txt_judul = response.txt_judul ?: "",
            txt_waktu = response.txt_waktu ?: "",
            txt_tempat = response.txt_tempat ?: "",
            txt_harga = response.txt_harga ?: "",
            txt_tanggal = response.txt_tanggal ?: "",
            txt_desc =  response.txt_desc ?: ""
        )

    companion object {
        const val TAG_EVENT = "TAG_EVENT"
    }
}
package com.nulfa.ventbookadmin

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.nulfa.ventbookadmin.databinding.RowRvConfTransactionBinding

class TicketAdapter : RecyclerView.Adapter<TicketAdapter.TicketViewHolder>() {
    private val data = arrayListOf<Ticket>()

    private lateinit var dbRef : DatabaseReference
    fun setData(newData: List<Ticket>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    inner class TicketViewHolder(private val view: RowRvConfTransactionBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(data: Ticket) {
            view.txtTitle.text=data.txt_judul
            view.txtJumlahTiket.text=data.txt_order
            view.txtMetodeTransaksi.text=data.methode_bayar
            view.txtJumlahTransaksi.text=data.txt_bayar
            view.txtNama.text="Husnul Khotimah"
            view.imgOk.setOnClickListener{
                dbRef = FirebaseDatabase.getInstance().getReference("Ticket")
                dbRef.child(data.id_user).child(data.status_bayar).child(data.kode_ticket).removeValue()
                data.status_bayar = "Success"
                dbRef.child(data.id_user).child(data.status_bayar).child(data.kode_ticket).setValue(data)
            }
            view.imgNo.setOnClickListener{
                dbRef = FirebaseDatabase.getInstance().getReference("Ticket")
                dbRef.child(data.id_user).child(data.status_bayar).child(data.kode_ticket).removeValue()
                data.status_bayar = "Failed"
                dbRef.child(data.id_user).child(data.status_bayar).child(data.kode_ticket).setValue(data)
            }
        }
    }

    override fun onBindViewHolder(holder: TicketViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketViewHolder =
        TicketViewHolder(
            RowRvConfTransactionBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
}
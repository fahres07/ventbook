package com.nulfa.ventbookadmin

import com.google.gson.annotations.SerializedName

data class TicketResponse(
    @field:SerializedName("kode_ticket")
    val kode_ticket: String? = null,
    @field:SerializedName("id_user")
    val id_user: String? = null,
    @field:SerializedName("txt_judul")
    val txt_judul: String? = null,
    @field:SerializedName("txt_waktu")
    val txt_waktu: String? = null,
    @field:SerializedName("txt_tempat")
    val txt_tempat: String? = null,
    @field:SerializedName("txt_order")
    val txt_order: String? = null,
    @field:SerializedName("status_bayar")
    val status_bayar: String? = null,
    @field:SerializedName("methode_bayar")
    val methode_bayar: String? = null,
    @field:SerializedName("txt_bayar")
    val txt_bayar: String? = null
)
package com.nulfa.ventbookadmin
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Event (
    var gambar: String,
    var txt_judul: String,
    var txt_waktu: String,
    var txt_harga: String,
    var txt_tempat: String,
    var txt_tanggal: String,
    var txt_desc: String
):Parcelable
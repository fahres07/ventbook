package com.nulfa.ventbookadmin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import com.nulfa.ventbookadmin.databinding.ActivityTransactionBinding

class TransaksiActivity : AppCompatActivity(){
    private val mAdapter = TicketSuccessAdapter()
    private lateinit var dbRef : DatabaseReference
    private lateinit var binding: ActivityTransactionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTransactionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnKonfirmasi.setOnClickListener {
            val intent = Intent(this, ConfirTransactionActivity::class.java)
            startActivity(intent)
        }

        try {
            dbRef = FirebaseDatabase.getInstance().reference
            dbRef.child("Ticket").addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val listTicket = arrayListOf<Ticket>()

                        snapshot.children.forEach { Ticket ->
                            Ticket.children.forEach { proses ->
                                proses.children.forEach {
                                    it.getValue(TicketResponse::class.java)?.let { response ->
                                        val transformedResponse = mapResponseToTicket(response)
                                        listTicket.add(transformedResponse)
                                    }
                                }

                            }
                        }
                        mAdapter.setData(listTicket.filter { it.status_bayar=="Success" })
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Toast.makeText(this@TransaksiActivity, error.message, Toast.LENGTH_SHORT).show()
                    }
                }
            )

        } catch (e:Exception){
            Log.d("debug", e.message.toString())
        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        with(binding.rvTransaction){
            layoutManager = LinearLayoutManager(this@TransaksiActivity)
            adapter=mAdapter
        }
    }

    private fun mapResponseToTicket(response: TicketResponse): Ticket =
        Ticket(
            kode_ticket = response.kode_ticket ?: "",
            id_user = response.id_user ?: "",
            txt_judul = response.txt_judul ?: "",
            txt_waktu = response.txt_waktu ?: "",
            txt_tempat = response.txt_tempat ?: "",
            txt_order = response.txt_order ?: "",
            status_bayar = response.status_bayar ?: "",
            methode_bayar =  response.methode_bayar ?: "",
            txt_bayar = response.txt_bayar ?: ""
        )
}
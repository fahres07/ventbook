package com.nulfa.ventbookadmin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.nulfa.ventbookadmin.EventActivity.Companion.TAG_EVENT
import com.nulfa.ventbookadmin.databinding.ActivityEditEventBinding

class EditEventActivity : AppCompatActivity() {
    private lateinit var database: DatabaseReference
    private lateinit var binding: ActivityEditEventBinding
    private lateinit var Event: Event
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditEventBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getParcelableExtra<Event>(TAG_EVENT)?.let {
            Event = it
            updateUI(it)
        }

        binding.btnSave.setOnClickListener {
            val txt_judul = binding.txtTitle.text.toString()
            val txt_tempat = binding.txtLokasi.text.toString()
            val txt_waktu = binding.txtWaktu.text.toString()
            val txt_harga = binding.txtHarga.text.toString()
            val txt_tanggal = binding.txtTanggal.text.toString()
            val txt_desc = binding.txtDeskripsi.text.toString()
            val gambar = binding.txtGambar.text.toString()
            Event = Event.copy(
                txt_judul = txt_judul,
                txt_tempat = txt_tempat,
                txt_waktu = txt_waktu,
                txt_harga = txt_harga,
                txt_tanggal = txt_tanggal,
                txt_desc = txt_desc,
                gambar = gambar
            )
            database = FirebaseDatabase.getInstance().getReference("Event")
            database.child(Event.txt_judul).setValue(Event)
                .addOnSuccessListener {
                binding.txtTitle.text.clear()
                binding.txtLokasi.text.clear()
                binding.txtWaktu.text.clear()
                binding.txtHarga.text.clear()
                binding.txtTanggal.text.clear()
                binding.txtGambar.text.clear()
                binding.txtDeskripsi.text.clear()

                Toast.makeText(this, "Success Edit Event", Toast.LENGTH_SHORT).show()
                onBackPressed()
            }.addOnFailureListener {
                Toast.makeText(this, "Failed Edit Event", Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun updateUI(event: Event) {
        binding.txtTitle.setText(event.txt_judul)
        binding.txtWaktu.setText(event.txt_waktu)
        binding.txtLokasi.setText(event.txt_tempat)
        binding.txtTanggal.setText(event.txt_tanggal)
        binding.txtHarga.setText(event.txt_harga)
        binding.txtGambar.setText(event.gambar)
        binding.txtDeskripsi.setText(event.txt_desc)
        binding.tvTitle.setText(event.txt_judul)
    }

}
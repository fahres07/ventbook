package com.nulfa.ventbookadmin
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import com.nulfa.ventbookadmin.databinding.ActivityConfirTransactionBinding


class ConfirTransactionActivity : AppCompatActivity() {
    private val mAdapter = TicketAdapter()
    private lateinit var dbRef : DatabaseReference
    private lateinit var binding: ActivityConfirTransactionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConfirTransactionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnDone.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        try {
            dbRef = FirebaseDatabase.getInstance().reference
            dbRef.child("Ticket").addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val listTicket = arrayListOf<Ticket>()

                        snapshot.children.forEach { Ticket ->
                            Ticket.children.forEach { proses ->
                                proses.children.forEach {
                                    it.getValue(TicketResponse::class.java)?.let { response ->
                                        val transformedResponse = mapResponseToTicket(response)
                                        listTicket.add(transformedResponse)
                                    }
                                }

                            }
                        }

                        mAdapter.setData(listTicket.filter { it.status_bayar=="On Proccess" })
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Toast.makeText(this@ConfirTransactionActivity, error.message, Toast.LENGTH_SHORT).show()
                    }

                }
            )
        } catch (e:Exception){
            Log.d("debug", e.message.toString())
        }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        with(binding.rvConfir){
            layoutManager = LinearLayoutManager(this@ConfirTransactionActivity)
            adapter=mAdapter
        }
    }

    private fun mapResponseToTicket(response: TicketResponse): Ticket =
        Ticket(
            kode_ticket = response.kode_ticket ?: "",
            id_user = response.id_user ?: "",
            txt_judul = response.txt_judul ?: "",
            txt_waktu = response.txt_waktu ?: "",
            txt_tempat = response.txt_tempat ?: "",
            txt_order = response.txt_order ?: "",
            status_bayar = response.status_bayar ?: "",
            methode_bayar =  response.methode_bayar ?: "",
            txt_bayar = response.txt_bayar ?: ""
        )


}
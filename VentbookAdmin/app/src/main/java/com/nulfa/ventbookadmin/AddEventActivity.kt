package com.nulfa.ventbookadmin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.nulfa.ventbookadmin.databinding.ActivityAddEventBinding

class AddEventActivity : AppCompatActivity() {
    lateinit var database : DatabaseReference
    private lateinit var binding : ActivityAddEventBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddEventBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSave.setOnClickListener {
            val txt_judul= binding.txtTitle.text.toString()
            val txt_tempat= binding.txtLokasi.text.toString()
            val txt_waktu= binding.txtWaktu.text.toString()
            val txt_harga= binding.txtHarga.text.toString()
            val txt_tanggal= binding.txtTanggal.text.toString()
            val txt_desc= binding.etDeskripsi.text.toString()
            val gambar=binding.txtGambar.text.toString()
            database = FirebaseDatabase.getInstance().getReference("Event")
            val Event = Event(gambar,txt_judul,txt_waktu,txt_harga,txt_tempat,txt_tanggal,txt_desc)
            database.child(txt_judul).setValue(Event).addOnSuccessListener {
                binding.txtTitle.text.clear()
                binding.txtHarga.text.clear()
                binding.txtLokasi.text.clear()
                binding.txtWaktu.text.clear()
                binding.txtTanggal.text.clear()
                binding.etDeskripsi.text.clear()
                binding.txtGambar.text.clear()

                Toast.makeText(this, "Success Add Event", Toast.LENGTH_SHORT).show()

            }.addOnFailureListener {
                Toast.makeText(this, "Failed Add Event", Toast.LENGTH_SHORT).show()
            }
        }

    }
}
package com.nulfa.ventbookadmin

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nulfa.ventbookadmin.databinding.RowRvTransactionBinding

class TicketSuccessAdapter : RecyclerView.Adapter<TicketSuccessAdapter.TicketSuccessViewHolder>(){
    private val data = arrayListOf<Ticket>()

    fun setData (newData : List<Ticket>){
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    inner class TicketSuccessViewHolder (private val view: RowRvTransactionBinding): RecyclerView.ViewHolder(view.root){
        fun bind(data : Ticket){
            view.txtTitle.text = data.txt_judul
            view.txtCode.text = data.methode_bayar
            view.txtJumlahTiket.text = data.txt_order + " Ticket"
        }
    }

    override fun onBindViewHolder(holder: TicketSuccessViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int =data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketSuccessViewHolder =
        TicketSuccessViewHolder(RowRvTransactionBinding.inflate(LayoutInflater.from(parent.context),parent,false))
}
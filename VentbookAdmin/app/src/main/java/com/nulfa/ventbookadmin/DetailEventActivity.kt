package com.nulfa.ventbookadmin
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.nulfa.ventbookadmin.EventActivity.Companion.TAG_EVENT
import com.nulfa.ventbookadmin.databinding.ActivityDetailEventBinding

class DetailEventActivity : AppCompatActivity() {
    private lateinit var binding : ActivityDetailEventBinding
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding= ActivityDetailEventBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getParcelableExtra<Event>(TAG_EVENT)?.let { updateUI(it) }

    }

    private fun updateUI(data : Event){
        binding.tvtitle.text = data.txt_judul
        binding.tvlocation.text = data.txt_tempat
        binding.tvdate.text = data.txt_tanggal
        binding.tvtime.text = data.txt_waktu
        binding.tvdesc.text= data.txt_desc
        binding.tvtiket.text="Rp."+data.txt_harga
        Glide.with(this).load(data.gambar).into(binding.cover)


    }
}